<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

## El Observador Api

Siga los siguientes pasos:


1. Clonar proyecto e ingresar al directorio
2. Ejecutar ```npm install```
3. Cree una base de datos en Postgres con el siguiente nombre NoticiasDB
4. Cambiar las variables de entorno del archivo .env (utilice los usuarios datos de conexion a la bd)
```
DB_HOST=localhost
DB_NAME=NoticiasDB
DB_PORT=5432
DB_USERNAME=postgres
DB_PASSWORD=root
JWT_SECRET=jnaskdjsdscx

```
5. Levantar el servicio: ```npm run start:dev```
6. Ejecutar SEED 
```
http://localhost:3000/seed
```
7. Utilizar los siguientes usuarios
```
Suscriptor:
Username: suscriptor
Password: suscriptor

Redactor 1:
Username: redactor
Password: redactor

Redactor 2:
Username: redactor2
Password: redactor2

Editor:
Username: editor
Password: editor

```