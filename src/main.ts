
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ResponseInterceptor } from './commons/interceptors/response.interceptor';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // interceptores
  app.useGlobalInterceptors(new ResponseInterceptor());
  // documentacion
  const config = new DocumentBuilder()
    .setTitle('Noticias Agetic')
    .setDescription('Api para manejo de noticias')
    .setVersion('1.0')
    .addTag('noticias')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // habilitar validaciones globales
  //app.useGlobalPipes(new ValidationPipe);

  // enbable cors
  app.enableCors();

  await app.listen(3000);
}
bootstrap();
