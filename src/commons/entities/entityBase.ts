import { BeforeInsert, BeforeUpdate, Column } from 'typeorm';

export class EntityBase {
  @Column({type: 'timestamptz' })
  createdAt?: Date;

  @Column({ type: 'timestamptz', nullable:true })
  updatedAt?: Date;

  @Column({ type: 'varchar', length: 25 })
  createdBy?: string;

  @Column({ type: 'varchar', length: 25, nullable:true })
  updatedBy?: string;

  @Column({ type: 'boolean', default: true })
  active?: boolean;

  @BeforeInsert()
  protected setCreatedAt(): void {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  protected setUpdatedAt(): void {
    this.updatedAt = new Date();
  }
}
