export interface ResponseDto {
  success: boolean;
  message: string;
  content?: any;
}
