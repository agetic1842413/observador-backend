export enum UserRole {
    SUSCRIPTOR = 'suscriptor',
    EDITOR = 'editor',
    REDACTOR = 'redactor'
}