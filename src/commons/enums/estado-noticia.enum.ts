export enum NoticiaEstado {
    EN_EDICION = 'e',
    PUBLICADO = 'p'
}