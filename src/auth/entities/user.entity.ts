import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('usuarios')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 25, unique: true })
  username: string;

  @Column({ type: 'text' })
  password: string;

  @Column({ type: 'varchar', length: 100 })
  nombreCompleto: string;

  @Column({ type:'bool', default: true })
  isActive: boolean;

  @Column('text', {
    array: true,
    default: ['suscriptor'],
  })
  roles: string[];

  @BeforeInsert()
  checkFieldsBeforeInsert() {}

  @BeforeUpdate()
  checkFieldsBeforeUpdate() {
    this.checkFieldsBeforeInsert();
  }
}
