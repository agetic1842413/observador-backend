import { IsString, Matches, MaxLength, MinLength } from 'class-validator';


export class CreateUserDto {

    @IsString()
    @MinLength(3, { message: 'El username debe tener el menos 3 caracteres' })
    @MaxLength(25)
    username: string;

    @IsString()
    @MinLength(6)
    @MaxLength(50)
    @Matches(
        /(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
        message: 'La contraseña debe tener una mayuscula, minuscula y un numero'
    })
    password: string;

    @IsString()
    @MinLength(1)
    nombreCompleto: string;
}