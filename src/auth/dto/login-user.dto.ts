import { IsString, MaxLength, MinLength } from 'class-validator';


export class LoginUserDto {

    @IsString()
    @MinLength(3)
    @MaxLength(25)
    username: string;

    @IsString()
    @MinLength(6)
    @MaxLength(50)
    password: string;

}