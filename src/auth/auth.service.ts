import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from './entities/user.entity';
import { LoginUserDto, CreateUserDto } from './dto';
import { JwtPayload } from './strategies/jwt-payload.interface';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,

    private readonly jwtService: JwtService,
  ) {}

  async create(createUserDto: CreateUserDto) {
    if (!createUserDto.username || createUserDto.username.length<3) {
      throw new BadRequestException('El username debe tener al menos 3 caracteres');
    }

    let existe = await this.userRepository.findOneBy({username: createUserDto.username});
    if (existe) throw new BadRequestException('Ya existe un usuario con el username enviado');
    
    const { password, ...userData } = createUserDto;
    const user = this.userRepository.create({
      ...userData,
      password: bcrypt.hashSync(password, 10),
    });

    await this.userRepository.save(user);
    delete user.password;

    return {
      ...user,
      token: this.getJwtToken({ username: user.username }),
    };
  }

  async login(loginUserDto: LoginUserDto) {
    const { password, username } = loginUserDto;

    const user = await this.userRepository.findOne({
      where: { username },
      select: { username: true, password: true, nombreCompleto: true, roles: true }, //! OJO!
    });

    if (!user)
      throw new UnauthorizedException('Credencial no válida (username)');

    if (!bcrypt.compareSync(password, user.password))
      throw new UnauthorizedException('Credencial no válida (password)');

    return {
      nombreCompleto: user.nombreCompleto,
      username: user.username,
      roles: user.roles,
      token: this.getJwtToken({ username: user.username }),
    };
  }

  async checkAuthStatus(user: User) {
    return {
      ...user,
      token: this.getJwtToken({ username: user.username }),
    };
  }

  private getJwtToken(payload: JwtPayload) {
    const token = this.jwtService.sign(payload);
    return token;
  }

  private handleDBErrors(error: any): never {
    console.log(error);
    if (error.code === '23505') throw new BadRequestException(error.detail);
    throw new InternalServerErrorException('Please check server logs');
  }
}
