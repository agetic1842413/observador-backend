import { Module } from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { NoticiasController, NoticiasPublicadasController } from './noticias.controller';
import { Noticia } from './entities/noticia.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([Noticia]), AuthModule],
  controllers: [NoticiasController, NoticiasPublicadasController],
  providers: [NoticiasService],
  exports: [
    NoticiasService,
    TypeOrmModule,
  ]
})
export class NoticiasModule {}
