import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
import { Auth, GetUser } from 'src/auth/decorators';
import { UserRole } from 'src/commons/enums/role.enum';
import { User } from 'src/auth/entities/user.entity';
import { BuscarNoticiaDto, BuscarNoticiaPublicadaDto } from './dto/find-noticia.dto';

@Controller('noticias')
export class NoticiasController {
  constructor(private readonly noticiasService: NoticiasService) {}

  @Post()
  @Auth( UserRole.REDACTOR )
  create(@Body() createNoticiaDto: CreateNoticiaDto, @GetUser() user: User) {
    return this.noticiasService.create(createNoticiaDto, user);
  }

  @Get(':id')
  @Auth( UserRole.EDITOR, UserRole.REDACTOR )
  findById(@Param('id') id: number) {
    return this.noticiasService.findOne(id);
  }

  @Get()
  @Auth( UserRole.EDITOR, UserRole.REDACTOR )
  findNewsByParams(@Query() params: BuscarNoticiaDto, @GetUser() user: User) {
    return this.noticiasService.findNewsByParams(params, user);
  }

  @Patch(':id')
  @Auth( UserRole.EDITOR, UserRole.REDACTOR )
  update(@Param('id') id: number, @Body() updateNoticiaDto: UpdateNoticiaDto, @GetUser() user: User) {
    return this.noticiasService.update(id, updateNoticiaDto, user);
  }

  @Patch('/publicar/:id')
  @Auth( UserRole.EDITOR, UserRole.REDACTOR )
  publicNew(@Param('id') id: number, @GetUser() user: User) {
    return this.noticiasService.publicNew(id, user);
  }

  @Delete(':id')
  @Auth( UserRole.EDITOR )
  remove(@Param('id') id: number, @GetUser() user: User) {
    return this.noticiasService.remove(id, user);
  }
}

@Controller('noticias-publicadas')
export class NoticiasPublicadasController {
  constructor(private readonly noticiasService: NoticiasService) {}
  @Get()
  findPublishByParams(@Query() params: BuscarNoticiaPublicadaDto) {
    return this.noticiasService.findPublishByParams(params);
  }
}