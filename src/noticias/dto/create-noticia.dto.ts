import {
  IsEnum,
    IsNotEmpty,
    IsString,
    MinLength,
  } from 'class-validator';
  
  export class CreateNoticiaDto {
    @IsString()
    @MinLength(2, { message: 'Debe ingresar al menos 2 caracteres.' })
    @IsNotEmpty()
    titulo: string;
  
    @IsString()
    @IsNotEmpty()
    lugar: string;

    @IsString()
    @IsNotEmpty()
    autor: string;
  
    @IsString()
    @IsNotEmpty()
    contenido: string;

    @IsString()
    @IsEnum(['e', 'p'])
    estado: string;
  }