import { IsBoolean, IsInt, IsNotEmpty, IsString } from 'class-validator';


export class BuscarNoticiaPublicadaDto {

  @IsInt()
  cantidad?: number;

  @IsString()
  campoOrdenacion?: string;

  @IsString()
  campoFiltro: string;

  @IsString()
  termino: string;
}

export class BuscarNoticiaDto extends BuscarNoticiaPublicadaDto{

  @IsBoolean()
  publicados?: boolean; 
}