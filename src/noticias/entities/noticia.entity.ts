import { EntityBase } from 'src/commons/entities/entityBase';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('noticias')
export class Noticia extends EntityBase {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 100, unique: true })
  titulo: string;

  @Column({ type: 'date', nullable: true })
  fechaPublicacion: Date;

  @Column({ type: 'varchar', length: 100 })
  lugar: string;

  @Column({ type: 'varchar', length: 100 })
  autor: string;

  @Column({ type: 'text' })
  contenido: string;

  @Column({ type: 'enum', enum: ['e', 'p'] })
  /**
   * e - editado
   * f - publicado
   */
  estado: string;
}
