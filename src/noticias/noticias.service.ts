import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
import { Noticia } from './entities/noticia.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/entities/user.entity';
import { BuscarNoticiaDto, BuscarNoticiaPublicadaDto } from './dto/find-noticia.dto';
import { NoticiaEstado } from 'src/commons/enums/estado-noticia.enum';
import { UserRole } from 'src/commons/enums/role.enum';

@Injectable()
export class NoticiasService {
  private readonly logger = new Logger('NoticiasService');
  constructor(
    @InjectRepository(Noticia)
    private readonly noticiaRepository: Repository<Noticia>,
  ) {}

  async create(createNoticiaDto: CreateNoticiaDto, user: User) {
    this.logger.log(createNoticiaDto);
    if (!createNoticiaDto.titulo) 
    throw new BadRequestException(
      'El titulo no debe ser vacio',
    );


    let existe = await this.noticiaRepository.findOneBy({
      titulo: createNoticiaDto.titulo,
    });
    if (existe) 
      throw new BadRequestException(
        'Ya existe una noticia con el titulo enviado',
      );
    

    const noticia = this.noticiaRepository.create(createNoticiaDto);
    noticia.createdBy = user.username;
    noticia.autor = user.nombreCompleto;
    noticia.estado = noticia.estado??NoticiaEstado.EN_EDICION;
    await this.noticiaRepository.save(noticia);
    return noticia;
  }

  async update(id: number, updateNoticiaDto: UpdateNoticiaDto, user: User) {
    const noticia = await this.findOne(id);
    noticia.titulo = updateNoticiaDto.titulo;
    noticia.autor = updateNoticiaDto.autor;
    noticia.contenido = updateNoticiaDto.contenido;
    noticia.lugar = updateNoticiaDto.lugar;
    noticia.updatedBy = user.username;
    await this.noticiaRepository.save(noticia);
    return noticia;
  }

  async remove(id: number, user: User) {
    const noticia = await this.findOne(id);
    noticia.active = false;
    noticia.updatedBy = user.username;
    await this.noticiaRepository.save(noticia);
    return noticia.id;
  }

  async findPublishByParams(params: BuscarNoticiaPublicadaDto) {
    const queryBuilder: SelectQueryBuilder<Noticia> = this.noticiaRepository
      .createQueryBuilder('noticia')
      .where('noticia.active = true').andWhere("noticia.estado='p'");
    if (params.termino) {
      queryBuilder.andWhere(
        ' (noticia.titulo ILIKE :termino or noticia.contenido ILIKE :termino) ',
        { termino: '%' + params.termino + '%' },
      );
    }
    if (params.campoOrdenacion) {
      queryBuilder.orderBy('noticia.' + params.campoOrdenacion, 'ASC');
    } else {
      queryBuilder.orderBy('noticia.fechaPublicacion', 'DESC');
    }
    if (params.cantidad) {
      queryBuilder.limit(params.cantidad);
    }
    return queryBuilder.getMany();
  }

  async findNewsByParams(params: BuscarNoticiaDto, user: User) {
    const queryBuilder: SelectQueryBuilder<Noticia> = this.noticiaRepository
      .createQueryBuilder('noticia')
      .where('noticia.active = true');
    if (params.publicados) {
      queryBuilder.andWhere("noticia.estado='p'");
    }
    if(user.roles.includes(UserRole.REDACTOR)){
      queryBuilder.andWhere("noticia.createdBy='"+user.username+"'");
    }
    if (params.termino) {
      queryBuilder.andWhere(
        ' (noticia.titulo ILIKE :termino or noticia.contenido ILIKE :termino) ',
        { termino: '%' + params.termino + '%' },
      );
    }

    queryBuilder.orderBy('noticia.id', 'DESC');

    return queryBuilder.getMany();
  }

  async findOne(id: number) {
    const noticia = await this.noticiaRepository.findOneBy({
      id,
      active: true,
    });
    if (!noticia)
      throw new NotFoundException(`No existe la noticia con el id: ${id}`);
    return noticia;
  }

  async publicNew(id: number, user: User) {
    const noticia = await this.findOne(id);
    if (noticia.estado===NoticiaEstado.PUBLICADO)
        throw new BadRequestException(`La noticia ya se encuentra publicada`);
    noticia.fechaPublicacion = new Date();
    noticia.estado = NoticiaEstado.PUBLICADO;
    noticia.updatedBy = user.username;
    await this.noticiaRepository.save(noticia);
    return noticia;
  }

  async deleteAll() {
    const query = this.noticiaRepository.createQueryBuilder('noticia');
    return await query.delete().where({}).execute();
  }
}
