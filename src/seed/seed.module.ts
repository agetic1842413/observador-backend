import { Module } from '@nestjs/common';
import { AuthModule } from './../auth/auth.module';
import { SeedService } from './seed.service';
import { SeedController } from './seed.controller';
import { NoticiasModule } from 'src/noticias/noticias.module';

@Module({
  controllers: [SeedController],
  providers: [SeedService],
  imports: [
    NoticiasModule,
    AuthModule
  ]
})
export class SeedModule {}
