import { Controller, Get, } from '@nestjs/common';

import { SeedService } from './seed.service';

@Controller('seed')
export class SeedController {
  constructor(private readonly seedService: SeedService) {}

  @Get()
  // @Auth( UserRole.REDACTOR )
  executeSeed() {
    return this.seedService.runSeed()
  }
}
