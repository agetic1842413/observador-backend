import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { initialData } from './data/seed-data';
import { User } from '../auth/entities/user.entity';
import { NoticiasService } from '../noticias/noticias.service';


@Injectable()
export class SeedService {

  private readonly logger = new Logger('SeedService');
  constructor(
    private readonly noticiasService: NoticiasService,
    @InjectRepository( User )
    private readonly usuarioRepository: Repository<User>
  ) {}

  async runSeed() {

    await this.limpiarTablas()
    const user = await this.insertarUsuarios();
    await this.insertarNoticias( user );
    return 'SEED EJECUTADO';
  }

  private async limpiarTablas() {
    await this.noticiasService.deleteAll();
    const queryBuilder = this.usuarioRepository.createQueryBuilder();
    await queryBuilder
      .delete()
      .where({})
      .execute();
  }

  private async insertarUsuarios() {
    const seedUsers = initialData.usuarios;
    const users: User[] = [];
    seedUsers.forEach( user => {
      users.push( this.usuarioRepository.create( user ) )
    });

    const dbUsers = await this.usuarioRepository.save( seedUsers )
    return dbUsers[0];
  }


  private async insertarNoticias( user: User ) {
    await this.noticiasService.deleteAll();
    const noticias = initialData.noticias;
    const insertPromises = [];
    noticias.forEach( noticia => {
      insertPromises.push( this.noticiasService.create( noticia, user ) );
    });

    await Promise.all( insertPromises );
    return true;
  }


}
