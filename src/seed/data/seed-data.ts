import * as bcrypt from 'bcrypt';

interface SeedNoticia {
  titulo: string;
  lugar: string;
  autor: string;
  contenido: string;
  fechaPublicacion: string;
  estado: string;
}

interface SeedUsuario {
  username: string;
  nombreCompleto: string;
  password: string;
  roles: string[];
}

interface SeedData {
  usuarios: SeedUsuario[];
  noticias: SeedNoticia[];
}

export const initialData: SeedData = {
  usuarios: [
    {
      nombreCompleto: 'Juan Peres',
      username: 'redactor',
      password: bcrypt.hashSync('redactor', 10),
      roles: ['redactor'],
    },
    {
      nombreCompleto: 'Pablo Gutierrez',
      username: 'redactor2',
      password: bcrypt.hashSync('redactor2', 10),
      roles: ['redactor'],
    },
    {
      nombreCompleto: 'Pablo Gomez',
      username: 'editor',
      password: bcrypt.hashSync('editor', 10),
      roles: ['editor'],
    },
    {
      nombreCompleto: 'Pablo Gutierrez',
      username: 'editor2',
      password: bcrypt.hashSync('editor2', 10),
      roles: ['editor'],
    },
    {
      nombreCompleto: 'Luis Gomez',
      username: 'suscriptor',
      password: bcrypt.hashSync('suscriptor', 10),
      roles: ['suscriptor'],
    },
  ],

  noticias: [
    {
      titulo: 'Nueva investigación sobre cambio climático',
      lugar: 'Estados Unidos',
      autor: 'John Smith',
      contenido:
        'Un nuevo estudio revela impactantes hallazgos sobre el cambio climático y su impacto en la biodiversidad.',
      fechaPublicacion: '2024-03-23',
      estado: 'p',
    },
    {
      titulo: 'Descubrimiento arqueológico en Egipto',
      lugar: 'Egipto',
      autor: 'Maria González',
      contenido:
        'Arqueólogos descubren una antigua tumba faraónica en el Valle de los Reyes, revelando secretos del antiguo Egipto.',
      fechaPublicacion: '2024-03-22',
      estado: 'p',
    },
    {
      titulo: 'Nuevas medidas económicas en Europa',
      lugar: 'Francia',
      autor: 'Jean Dupont',
      contenido:
        'El gobierno francés anuncia un paquete de medidas económicas para estimular el crecimiento y la inversión en el país.',
      fechaPublicacion: '2024-03-21',
      estado: 'p',
    },
    {
      titulo: 'Avance médico en tratamiento del cáncer',
      lugar: 'Reino Unido',
      autor: 'Emily Johnson',
      contenido:
        'Investigadores británicos reportan avances prometedores en el desarrollo de terapias más efectivas contra el cáncer.',
      fechaPublicacion: '2024-03-20',
      estado: 'p',
    },
    {
      titulo: 'Nuevo récord de ventas de vehículos eléctricos',
      lugar: 'Alemania',
      autor: 'Hans Müller',
      contenido:
        'Alemania registra un aumento récord en las ventas de vehículos eléctricos, impulsando la transición hacia la movilidad sostenible.',
      fechaPublicacion: '2024-03-19',
      estado: 'p',
    },
    {
      titulo: 'Escándalo político en América Latina',
      lugar: 'Brasil',
      autor: 'Ana Silva',
      contenido:
        'Un nuevo escándalo político sacude Brasil, generando protestas y llamados a la transparencia en el gobierno.',
      fechaPublicacion: '2024-03-18',
      estado: 'p',
    },
    {
      titulo: 'Lanzamiento de nuevo satélite de observación',
      lugar: 'Japón',
      autor: 'Takeshi Yamamoto',
      contenido:
        'Japón pone en órbita un nuevo satélite de observación que promete mejorar la monitorización ambiental y la predicción de desastres naturales.',
      fechaPublicacion: '2024-03-17',
      estado: 'p',
    },
    {
      titulo: 'Inauguración de nueva planta de energía solar',
      lugar: 'España',
      autor: 'Luis García',
      contenido:
        'España inaugura una nueva planta de energía solar, marcando un hito en la transición hacia fuentes de energía renovable.',
      fechaPublicacion: '2024-03-16',
      estado: 'p',
    },
    {
      titulo: 'Desarrollo de nueva vacuna contra enfermedad tropical',
      lugar: 'África',
      autor: 'Fatima Diallo',
      contenido:
        'Científicos africanos anuncian avances significativos en el desarrollo de una vacuna contra una enfermedad tropical devastadora.',
      fechaPublicacion: '2024-03-15',
      estado: 'p',
    },
    {
      titulo: 'Cumbre internacional sobre seguridad alimentaria',
      lugar: 'Italia',
      autor: 'Giuseppe Rossi',
      contenido:
        'Líderes mundiales se reúnen en Italia para discutir estrategias destinadas a abordar la seguridad alimentaria global.',
      fechaPublicacion: '2024-03-14',
      estado: 'p',
    },
    {
      titulo: 'Hallazgo de especie marina desconocida',
      lugar: 'Australia',
      autor: 'Emma White',
      contenido:
        'Científicos descubren una nueva especie marina en las profundidades del océano Austral, destacando la importancia de la conservación marina.',
      fechaPublicacion: '2024-03-13',
      estado: 'p',
    },
    {
      titulo: 'Avances en inteligencia artificial en China',
      lugar: 'China',
      autor: 'Wei Zhang',
      contenido:
        'China anuncia importantes avances en inteligencia artificial, impulsando la innovación y la competitividad tecnológica a nivel mundial.',
      fechaPublicacion: '2024-03-12',
      estado: 'p',
    },
    {
      titulo: 'Incremento de tensiones en Medio Oriente',
      lugar: 'Israel/Palestina',
      autor: 'Mohammed Ali',
      contenido:
        'Las tensiones aumentan en la región de Medio Oriente tras una serie de incidentes violentos entre Israel y Palestina.',
      fechaPublicacion: '2024-03-11',
      estado: 'p',
    },
    {
      titulo: 'Lanzamiento de nueva plataforma de streaming',
      lugar: 'Estados Unidos',
      autor: 'Sarah Johnson',
      contenido:
        'Una nueva plataforma de streaming llega al mercado, ofreciendo una amplia variedad de contenido original y películas clásicas.',
      fechaPublicacion: '2024-03-10',
      estado: 'p',
    },
    {
      titulo: 'Descubrimiento de nueva especie de dinosaurio',
      lugar: 'Argentina',
      autor: 'Diego Fernandez',
      contenido:
        'Paleontólogos argentinos descubren restos fósiles de una nueva especie de dinosaurio, arrojando luz sobre la prehistoria del país.',
      fechaPublicacion: '2024-03-09',
      estado: 'p',
    },
    {
      titulo: 'Implementación de medidas de conservación marina',
      lugar: 'Canadá',
      autor: 'Emily Brown',
      contenido:
        'Canadá anuncia la implementación de nuevas medidas de conservación marina para proteger los ecosistemas costeros y la biodiversidad.',
      fechaPublicacion: '2024-03-08',
      estado: 'p',
    },
    {
      titulo: 'Desarrollo de nueva tecnología de almacenamiento de energía',
      lugar: 'Suiza',
      autor: 'Andreas Müller',
      contenido:
        'Investigadores suizos presentan una tecnología revolucionaria de almacenamiento de energía que podría transformar la industria energética.',
      fechaPublicacion: '2024-03-07',
      estado: 'p',
    },
    {
      titulo: 'Brotes de enfermedades infecciosas en Asia',
      lugar: 'Tailandia',
      autor: 'Nathaniel Wong',
      contenido:
        'Tailandia enfrenta brotes de enfermedades infecciosas, generando preocupaciones sobre la salud pública y la seguridad sanitaria regional.',
      fechaPublicacion: '2024-03-06',
      estado: 'p',
    },
    {
      titulo: 'Protestas por reformas laborales en Sudamérica',
      lugar: 'Chile',
      autor: 'Alejandra Martinez',
      contenido:
        'Trabajadores chilenos marchan en protesta por reformas laborales propuestas, exigiendo mejores condiciones y derechos laborales.',
      fechaPublicacion: '2024-03-05',
      estado: 'p',
    },
    {
      titulo: 'Desarrollo de nueva terapia génica',
      lugar: 'Estados Unidos',
      autor: 'Michael Brown',
      contenido:
        'Científicos estadounidenses anuncian avances prometedores en el desarrollo de terapias génicas para tratar enfermedades genéticas raras.',
      fechaPublicacion: '2024-03-04',
      estado: 'p',
    },
    {
      titulo: 'Lanzamiento de nuevo dispositivo móvil',
      lugar: 'Corea del Sur',
      autor: 'Ji-hyun Kim',
      contenido:
        'Una empresa surcoreana lanza al mercado un nuevo dispositivo móvil con características innovadoras y tecnología de vanguardia.',
      fechaPublicacion: '2024-03-03',
      estado: 'p',
    },
    {
      titulo: 'Aumento de la actividad volcánica en el Pacífico',
      lugar: 'Indonesia',
      autor: 'Dewi Suryani',
      contenido:
        'Indonesia experimenta un aumento significativo en la actividad volcánica, generando preocupaciones sobre la seguridad de la población local.',
      fechaPublicacion: '2024-03-02',
      estado: 'p',
    },
    {
      titulo: 'Desarrollo de nueva vacuna contra enfermedad viral',
      lugar: 'Alemania',
      autor: 'Klaus Schmidt',
      contenido:
        'Investigadores alemanes anuncian progresos en el desarrollo de una vacuna efectiva contra una enfermedad viral altamente contagiosa.',
      fechaPublicacion: '2024-03-01',
      estado: 'p',
    },
    {
      titulo: 'Celebración de festival cultural en India',
      lugar: 'India',
      autor: 'Priya Patel',
      contenido:
        'India celebra un festival cultural colorido y vibrante, atrayendo a visitantes de todo el mundo para disfrutar de música, danza y gastronomía.',
      fechaPublicacion: '2024-02-29',
      estado: 'p',
    },
    {
      titulo: 'Lanzamiento de misión espacial a Marte',
      lugar: 'Estados Unidos',
      autor: 'David Johnson',
      contenido:
        'Estados Unidos anuncia el lanzamiento de una nueva misión espacial a Marte, con el objetivo de explorar el planeta rojo en busca de signos de vida pasada.',
      fechaPublicacion: '2024-02-28',
      estado: 'p',
    },
    {
      titulo: 'Inauguración de nueva línea de tren de alta velocidad',
      lugar: 'Francia',
      autor: 'Sophie Leclerc',
      contenido:
        'Francia inaugura una nueva línea de tren de alta velocidad, conectando ciudades clave y mejorando la eficiencia del transporte ferroviario en el país.',
      fechaPublicacion: '2024-02-27',
      estado: 'p',
    },
    {
      titulo: 'Desarrollo de tecnología de purificación de agua',
      lugar: 'Kenia',
      autor: 'James Mwangi',
      contenido:
        'Ingenieros kenianos desarrollan una tecnología innovadora de purificación de agua que podría ayudar a abordar la escasez de agua potable en la región, el cual estara disponible para la venta en 2025.',
      fechaPublicacion: '2024-02-26',
      estado: 'p',
    },
    {
      titulo: 'Debate sobre reforma educativa en Europa',
      lugar: 'Alemania',
      autor: 'Hannah Fischer',
      contenido:
        'Alemania lidera un debate en la Unión Europea sobre la necesidad de reformas educativas para adaptarse a los desafíos del siglo XXI.',
      fechaPublicacion: '2024-02-25',
      estado: 'p',
    },
    {
      titulo: 'Descubrimiento de nueva especie de planta',
      lugar: 'Brasil',
      autor: 'Carlos Silva',
      contenido:
        'Botánicos brasileños descubren una nueva especie de planta en la selva amazónica, resaltando la importancia de la conservación de la biodiversidad.',
      fechaPublicacion: '2024-02-24',
      estado: 'p',
    },
    {
      titulo: 'Aprobación de ley de igualdad de género',
      lugar: 'Suecia',
      autor: 'Erika Andersson',
      contenido:
        'Suecia aprueba una ley de igualdad de género histórica, promoviendo la equidad y la inclusión en todos los ámbitos de la sociedad.',
      fechaPublicacion: '2024-02-23',
      estado: 'p',
    },
    {
      titulo: 'Descubrimiento de antigua civilización perdida',
      lugar: 'Grecia',
      autor: 'Nikos Papadopoulos',
      contenido:
        'Arqueólogos griegos descubren los restos de una antigua civilización perdida, arrojando nueva luz sobre la historia del Mediterráneo.',
      fechaPublicacion: '2024-02-22',
      estado: 'p',
    },
  ],
};
