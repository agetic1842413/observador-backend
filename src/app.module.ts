import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { NoticiasModule } from './noticias/noticias.module';
import { AuthModule } from './auth/auth.module';
import { SeedModule } from './seed/seed.module';
import { AppController } from './app.controller';


@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      password: process.env.DB_PASSWORD,
      username: process.env.DB_USERNAME,
      entities: [],
      database: process.env.DB_NAME,
      autoLoadEntities:true,
      synchronize: true,
      //logging: true,
    }),
    NoticiasModule,
    AuthModule,
    SeedModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
